﻿using StackExchange.Redis;

namespace MasivApi.Repositories
{
    public interface IRepository
    {
        HashEntry[] Get(string key);
        string FindById(string key, string fieldId);
        bool Post(string hasKey, string fieldKey, string value);
    }
}