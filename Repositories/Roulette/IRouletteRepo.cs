﻿using System;
using System.Collections.Generic;
using MasivApi.Models;

namespace MasivApi.Repositories
{
    public interface IRouletteRepo
    {
        List<Roulette> FindAll();
        Roulette FindById(Guid rouleGuid);
        bool AddRoullete(Roulette roulette);
        bool InitializeRoulette(Roulette roulette);
        bool FinalizeRoulette(Guid rouleGuid);
    }
}