﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasivApi.Enums;
using MasivApi.Models;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace MasivApi.Repositories
{
    public class RouletteRepo: IRouletteRepo
    {
        private readonly IRepository _repository;
        private readonly string RouKey = Environment.GetEnvironmentVariable("RouKey");

        public RouletteRepo(IRepository repository)
        {
            _repository = repository;

        }
        public List<Roulette> FindAll()
        {
            HashEntry[] result = _repository.Get(RouKey);
            if (result.Length <= 0)
                return new List<Roulette>();
            return ConverHasToList(result);
        }
        public Roulette FindById(Guid rouleGuid)
        {
            string result = _repository.FindById(RouKey, rouleGuid.ToString());
            if (string.IsNullOrEmpty(result))
            {
                throw new Exception($"Roulette Code => {rouleGuid} not found ");
            }

            return JsonConvert.DeserializeObject<Roulette>(result);
        }


        public bool AddRoullete(Roulette roulette)
        {
            string rouletteJson = JsonConvert.SerializeObject(roulette);
            return _repository.Post(RouKey, roulette.Id.ToString(), rouletteJson);
        }

        public bool InitializeRoulette(Roulette roulette)
        {
            ValidateIniziatileRoulette(roulette.Id);
            return AddRoullete(roulette);
        }

        public bool FinalizeRoulette(Guid rouleGuid)
        {
            var roulette = FindById(rouleGuid);
            if (roulette.Status == StatusRoulette.Close)
            {
                throw new Exception($"Sorry, the roulette Number => {rouleGuid} is closed");
            }
            roulette.Status = StatusRoulette.Close;
            return AddRoullete(roulette);
        }
        private List<Roulette> ConverHasToList(HashEntry[] entries)
        {
            List<Roulette> roulettes = new List<Roulette>();
            for (int i = 0; i < entries.Length; i++)
            {
                string data = entries[i].Value;
                roulettes.Add(JsonConvert.DeserializeObject<Roulette>(data));
            }
            return roulettes;
        }
        private void ValidateIniziatileRoulette(Guid rouleGuid)
        {
            List<Roulette> rouletes = FindAll();
            if (rouletes.Any())
            {
                Roulette rouletteOpenedExists = rouletes.FirstOrDefault(x => x.Status == StatusRoulette.Open);
                if (rouletteOpenedExists.Id == rouleGuid)
                {
                    throw new Exception("This Roulette was previous opened");
                }
                if (rouletteOpenedExists != null)
                {
                    throw new Exception("Please before open a new roulette, close all roulettes");
                }
            }
        }
    }
}