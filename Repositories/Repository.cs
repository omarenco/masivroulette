﻿using StackExchange.Redis;

namespace MasivApi.Repositories
{
    public class Repository: IRepository
    {
        private readonly IDatabase _dbCache;

        public Repository(IDatabase db)
        {
            _dbCache = db;

        }
        public HashEntry[] Get(string key)
        {
            return _dbCache.HashGetAll(key);
        }
        public string FindById(string key, string fieldId)
        {
            string result = string.Empty;
            var redisVal = _dbCache.HashGet(key, fieldId);
            if (redisVal != RedisValue.Null)
            {
                result = redisVal.ToString();
            }
            return result;
        }
        public bool Post(string hasKey, string fieldKey, string value)
        {
            return _dbCache.HashSet(hasKey, fieldKey, value);
        }
    }
}