﻿using System;
using System.Collections.Generic;
using System.Linq;
using MasivApi.Enums;
using MasivApi.Models;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace MasivApi.Repositories
{
    public class BetRepo: IBetRepo
    {
        readonly IRepository _repository;
        readonly IRouletteRepo _rouletteRepo;
        private readonly string BetKey = Environment.GetEnvironmentVariable("BetKey");
        private ResultBet resultBets = null;

        public bool RegisterBed(Bet bet)
        {
            if (!IsValidRoulette(bet.RoulleteId))
            {
                throw new Exception("Roulete not exist, please try again");
            }

            if (!ValidateColor(bet.BedColor))
            {
                throw new Exception("The colors availables are Red and Black");
            }

            string betJson = JsonConvert.SerializeObject(bet);
            string _fieldKey = $"{bet.RoulleteId}:{bet.Id}";
            return _repository.Post(BetKey, _fieldKey, betJson ); 

        }

        public ResultBet closeBet(Guid rouletteId)
        {
            var betsRoulette = GetAllByRouletteId(rouletteId: rouletteId);
            _rouletteRepo.FinalizeRoulette(rouletteId);
            setTheWinner(betsList: betsRoulette);

            return resultBets;
        }

        private List<Bet> GetAllByRouletteId(Guid rouletteId)
        {
            HashEntry[] entries = _repository.Get(BetKey);
            if (entries.Length <= 0)
                return new List<Bet>();

            List<Bet> Bets = ConverHasToList(entries: entries);

            return (Bets.Where(b => b.RoulleteId == rouletteId).ToList());
        }

        private bool IsValidRoulette(Guid rouletteId)
        {
            Roulette roulette = _rouletteRepo.FindById(rouletteId);

            return (roulette.Status == StatusRoulette.Open);
        }

        private List<Bet> ConverHasToList(HashEntry[] entries)
        {
            List<Bet> bets = new List<Bet>();
            for (int i = 0; i < entries.Length; i++)
            {
                string data = entries[i].Value;
                bets.Add(JsonConvert.DeserializeObject<Bet>(data));
            }
            return bets;
        }

        private bool ValidateColor(string color)
        {
            if (string.IsNullOrEmpty(color))
                return false;
            switch (color.ToLower())
            {
                case "Black":
                case "Red":

                    return true;
                default:

                    return false;
            }
        }
        private void setTheWinner(List<Bet> betsList)
        {
            List<Bet> betsByNumber = betsList.Where(x => x.BetNumber != 0).ToList();
            resultBets = new ResultBet();
            int index = new Random().Next(betsByNumber.Count);
            Bet winningBet = betsByNumber[index];
            string winningColor = (winningBet.BetNumber % 2 == 0 ? "Red" : "Black");
            resultBets.WinningColor = winningColor;
            resultBets.WinningNumber = winningBet.BetNumber;
            resultBets.Rewards.AddRange(CalculateProfitNumber(betsList));
            resultBets.Rewards.AddRange(CalculateProfitColor(betsList));
        }
        private List<Reward> CalculateProfitNumber(List<Bet> betsList)
        {
            double GainFactorPerNumber = Convert.ToDouble(Environment.GetEnvironmentVariable("GainFactorPerNumber"));

            return betsList.Where(b => b.BetNumber == resultBets.WinningNumber)
                .Select(c => new Reward()
                {
                    BetNumber = c.BetNumber,
                    BetAmount = c.BetNumber,
                    Total = c.BetNumber * GainFactorPerNumber
                }).ToList();
        }
        private List<Reward> CalculateProfitColor(List<Bet> betsList)
        {
            double GainFactorPerColor = Convert.ToDouble(Environment.GetEnvironmentVariable("GainFactorPerColor"));

            return betsList.Where(b => b.BedColor == resultBets.WinningColor)
                .Select(c => new Reward()
                {
                    BetColor = c.BedColor,
                    BetAmount = c.BetNumber,
                    Total = c.BetNumber * GainFactorPerColor
                }).ToList();
        }
    }
}