﻿using System;
using MasivApi.Models;

namespace MasivApi.Repositories
{
    public interface IBetRepo
    {
        bool RegisterBed(Bet bet);
        ResultBet closeBet(Guid rouletteId);
    }
}