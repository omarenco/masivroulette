﻿using System.Collections.Generic;

namespace MasivApi.Models
{
    public class ResultBet
    {
        public List<Reward> Rewards { get; set; }
        public int WinningNumber { get; set; }
        public string WinningColor { get; set; }
        public ResultBet()
        {
            Rewards = new List<Reward>();
        }
    }
}