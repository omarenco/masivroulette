﻿namespace MasivApi.Models
{
    public class Reward
    {
        public int BetNumber { get; set; }
        public string BetColor { get; set; }
        public double BetAmount { get; set; }
        public double Total { get; set; }
    }
}