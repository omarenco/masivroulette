﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MasivApi.Models
{
    public class Bet
    {
        public Guid Id { get; set; }
        [Range(0, 36, ErrorMessage = "The parameter number is between wrong range")]
        public int BetNumber { get; set; }
        public string BedColor { get; set; }
        [Range(0, 10000, ErrorMessage = "The Bet Amount Exceed limit")]
        private double BetAmount { get; set; }
        public Guid RoulleteId { get; set; }
        public string UserId { get; set; }
    }
}
