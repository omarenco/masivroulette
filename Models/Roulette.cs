﻿using System;
using MasivApi.Enums;

namespace MasivApi.Models
{
    public class Roulette
    {
        public Guid Id { get; set; }
        public StatusRoulette Status { get; set; }
        public DateTime CreateAt { get; set; }
    }
}
