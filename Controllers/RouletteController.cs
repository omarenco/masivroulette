﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasivApi.Enums;
using MasivApi.Models;
using MasivApi.Repositories;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MasivApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RouletteController : ControllerBase
    {
        private readonly IRouletteRepo _repository;
        public RouletteController(IRouletteRepo repository)
        {
            _repository = repository;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Roulette>>> Get()
        {
            IEnumerable<Roulette> roulettes = null;
            await Task.Run(() =>
            {
                roulettes = _repository.FindAll();
            });
            if (!roulettes.Any())
            {
                return NotFound();
            }

            return Ok(roulettes);

        }
        [HttpPost]
        public  async Task<ActionResult<Roulette>> Post()
        {
            bool successfull = false; 
            Roulette roulette = new Roulette {Id = Guid.NewGuid(), CreateAt = DateTime.UtcNow};
            await Task.Run(() =>
            {
                successfull = _repository.AddRoullete(roulette);
            });
            if (!successfull)
            {
                return BadRequest();
            }
            return Ok(roulette.Id.ToString());

        }

        [HttpPost("Initialize/{rouletteId:Guid}")]
        public async Task<ActionResult<Roulette>> Initialize(Guid rouletteId)
        {
            bool initialized = false;
            Roulette roulette = _repository.FindById(rouletteId);
            roulette.Status = StatusRoulette.Open;
            await Task.Run(() =>
            {
                initialized = !_repository.InitializeRoulette(roulette);
            });
            if (initialized)
            {
                return Ok(roulette);
            }
            else
            {
                throw new Exception("The roulette can't be initialized");
            }
        }
    }
}
